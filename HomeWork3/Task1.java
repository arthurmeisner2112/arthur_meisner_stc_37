import java.util.Arrays;
import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		
		System.out.println("Select Subprogram");
		System.out.println("1. Sum of Array");
		System.out.println("2. Reverse of Array");
		System.out.println("3. Average of Array");
		System.out.println("4. Min - Max Swap");
		System.out.println("5. Bubble sorting");
		System.out.println("6. Array to Number");
		System.out.println("0. To Exit");

		Scanner scanner = new Scanner(System.in);
		int select = scanner.nextInt();

		if (select == 1) {
			sumOfArray();
		}
		if (select == 2) {
			reverseOfArray();
		}
		if (select == 3) {
			averageOfArray();
		}
		if (select == 4) {
			minMaxSwap();
		}
		if (select == 5) {
			bubbleSorting();
		}
		if (select == 6) {
			arrayToNumber();
		}
		if (select == 0) {
			System.exit(0);
		}

	}

	public static void sumOfArray() {

		System.out.println("Insert Array length ");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];
		int arraySum = 0;



		for (int i = 0; i < array.length; i++) {
			 System.out.println("Insert next Number ");

			array[i] = scanner.nextInt();

		}

		for (int j = array.length - 1; j >= 0; j--) {

			arraySum = arraySum + array[j];
		}

		System.out.println("Sum of numbers is " + arraySum);

	}

	public static void reverseOfArray() {

		System.out.println("Insert Array length ");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];

			for (int i = 0; i < array.length; i++) {
			System.out.println("Insert next Number ");

			array[i] = scanner.nextInt();

		}

		System.out.println("Initial array is " + Arrays.toString(array));

		int arrayMiddle = array.length / 2;

		for (int j = 0; j < arrayMiddle; j++){

			int mirrorIndex = array.length -j - 1;
			int temp = array[mirrorIndex];
			array[mirrorIndex] = array[j];
			array[j] = temp;
		}

		System.out.println("Reversed array is " + Arrays.toString(array));
	}

	public static void averageOfArray() {

		System.out.println("Insert Array length ");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];
		double arrayAverage = 0;

			for (int i = 0; i < array.length; i++) {
			System.out.println("Insert next Number ");

			array[i] = scanner.nextInt();

		}

		double arraySum = 0;

		for (int j = 0; j < array.length; j++) {
			
			arraySum = arraySum + array[j];

		}

		arrayAverage = arraySum / array.length;

		System.out.println("Average of Array is " + arrayAverage);
	}

	public static void minMaxSwap() {

		System.out.println("Insert Array length ");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];


		for (int i = 0; i < array.length; i++) {
			 System.out.println("Insert next Number ");

			array[i] = scanner.nextInt();
		}

		System.out.println("Initial array is " +  Arrays.toString(array));

		int min = array[0];
		int positionOfMin = 0;
		int max = array[0];
		int positionOfMax = 0;

		for (int j = 0; j < array.length; j++) {
			if (array[j] < min) {
				min = array[j];
				positionOfMin = j;
			}
			if (array[j] > max) {
				max = array[j];
				positionOfMax = j;
			}
		}

		int temp = array[positionOfMin];
		array[positionOfMin] = array[positionOfMax];
		array[positionOfMax] = temp;


		System.out.println("Minimal array value is " + min);
		System.out.println("Maximal array value is " + max);
		System.out.println("Final array is " +  Arrays.toString(array));
	}

	public static void bubbleSorting() {

		System.out.println("Insert Array length ");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];


		for (int i = 0; i < array.length; i++) {
			 System.out.println("Insert next Number ");

			array[i] = scanner.nextInt();
		}
				System.out.println("Initial array is " + Arrays.toString(array));

		int temp;

		for (int j = 0; j < array.length - 1; j++) {

			for (int k = 0; k < array.length - 1 -j; k++) {
				if (array[k] > array[k + 1]) {

					temp = array[k];
					array[k] = array[k + 1];
					array[k + 1] = temp;
					System.out.println("Array in Sorting " + Arrays.toString(array));
				}
			}
		}
		System.out.println("Sorted array is " + Arrays.toString(array));
	}

	public static void arrayToNumber() {
// Эта подпрограмма работает только для чисел от 0 до 9

		System.out.println("Insert Array length ");		
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];
		System.out.println("Insert number in range 0 to 9 ");
		int k = 0;

			for (int i = 0; i < array.length; i++) {
				
				
			System.out.println("Insert next Number ");


			k = scanner.nextInt();
				if (k > 9) {
					System.err.println("Out of range. Insert right Number");
					k = scanner.nextInt();
						if (k > 9) {
							System.err.println("I'm Leaving, bye."); // Вы расстроили программу
							System.exit(0);
						}
						else {
						array[i] = k;
						}
				}

				else {
					array[i] = k;
				}


		}

		System.out.println("Initial array is " + Arrays.toString(array));

		int number = 0;

			for (int j = 0; j <array.length; j++){

				number = number * 10;
				number = number + array[j];
				
			}

			System.out.println("Array to Number is " + number);
	}
}
