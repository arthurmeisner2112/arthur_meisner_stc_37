package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Game {
    private Long id;
    private LocalDateTime dateTime;
    private Player firstPlayer;
    private Player secondPlayer;
    private Integer firstPlayerShotsCount;
    private Integer secondPlayerShotsCount;
    private Long secondsTimeAmount;
}