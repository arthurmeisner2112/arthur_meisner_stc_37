package ru.inno.game.repositories;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private static final String SQL_INSERT_PLAYER = "insert into player (ip, name, points, wins, loses) values (?,?,?,?,?)";
    //language=SQL
    private static final String SQL_UPDATE_PLAYER = "update player set ip = ?, points = ?, wins = ?, loses = ? where id =?";
    //language=SQL
    private static final String SQL_FIND_BY_NICKNAME = "select * from player where name = ?";

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Player> playerRowMapper = row -> Player.builder()
            .id(row.getLong("id"))
            .ip(row.getString("ip"))
            .name(row.getString("name"))
            .points(row.getInt("points"))
            .winsCounts(row.getInt("wins"))
            .loseCounts(row.getInt("loses"))
            .build();

    @Override
    public Player findByNickname(String nickName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME);) {
            statement.setString(1, nickName);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return playerRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);

        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getWinsCounts());
            statement.setInt(5, player.getLoseCounts());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                throw new SQLException("Can't  insert");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()){
                if (generatedKeys.next()) {
                    Long id = generatedKeys.getLong("id");
                    player.setId(id);
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            } catch (SQLException e){
                throw new IllegalArgumentException(e);
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);

        }
    }


    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PLAYER)) {
            statement.setString(1, player.getIp());
            statement.setInt(2, player.getPoints());
            statement.setInt(3, player.getWinsCounts());
            statement.setInt(4, player.getLoseCounts());
            statement.setLong(5, player.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
