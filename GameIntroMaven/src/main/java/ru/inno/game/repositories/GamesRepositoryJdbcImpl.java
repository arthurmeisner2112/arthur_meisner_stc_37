package ru.inno.game.repositories;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

public class GamesRepositoryJdbcImpl implements GamesRepository {
    //language=SQL
   //private final static String SQL_FIND_BY_ID = "select * from game full join player p on p.id = game.first_player full join player p2 on p2.id = game.second_player where game.id = ?";
   private final static String SQL_FIND_BY_ID = "select * from game full join player first  on first.id = game.first_player full join player second on second.id = game.second_player where game.id = ?";
    //language=SQL
    private final static String SQL_FIND_PLAYER_BY_ID = "select * from player where id = ?";
    //language=SQL
    private static final String SQL_UPDATE_GAME = "update game set seconds_time_amount = ?, first_player_shots = ?, second_player_shots = ? where id = ?";
    //language=SQL
    private final static String SQL_INSERT_GAME = "insert into game (date_time, first_player, second_player, first_player_shots, second_player_shots, seconds_time_amount) values (?,?,?,?,?,?)";

    private DataSource dataSource;

    private RowMapper<Game> gameRowMapper = row -> Game.builder()
            .id(row.getLong("id"))
            .dateTime(LocalDateTime.parse(row.getString("date_time")))
            .firstPlayer(Player.builder()
                    .id(row.getLong("first_player"))
                    .ip(row.getString(9))
                    .name(row.getString(10))
                    .points(row.getInt(11))
                    .winsCounts(row.getInt(12))
                    .loseCounts(row.getInt(13))
                    .build())
            .secondPlayer(Player.builder()
                    .id(row.getLong("second_player"))
                    .ip(row.getString(15))
                    .name(row.getString(16))
                    .points(row.getInt(17))
                    .winsCounts(row.getInt(18))
                    .loseCounts(row.getInt(19))
                    .build())
            .firstPlayerShotsCount(row.getInt("first_player_shots"))
            .secondPlayerShotsCount(row.getInt("second_player_shots"))
            .secondsTimeAmount(row.getLong("seconds_time_amount"))
            .build();

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getDateTime().toString());
            statement.setLong(2, game.getFirstPlayer().getId());
            statement.setLong(3, game.getSecondPlayer().getId());
            statement.setInt(4, game.getFirstPlayerShotsCount());
            statement.setInt(5, game.getSecondPlayerShotsCount());
            statement.setLong(6, game.getSecondsTimeAmount());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                throw new SQLException("Can't  insert");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()){
                if (generatedKeys.next()) {
                    Long id = generatedKeys.getLong("id");
                    game.setId(id);
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            } catch (SQLException e){
                throw new IllegalArgumentException(e);
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);

        }

    }

    @Override
    public Game findById(Long gameId) {


        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setLong(1, gameId);

            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                   Game game = gameRowMapper.mapRow(rows);
                   return game;
                }

            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);

        }
        return null;

    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME)) {
            statement.setLong(1, game.getSecondsTimeAmount());
            statement.setInt(2, game.getFirstPlayerShotsCount());
            statement.setInt(3, game.getSecondPlayerShotsCount());
            statement.setLong(4, game.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }
}

