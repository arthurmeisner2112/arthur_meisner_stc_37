package ru.inno.game.services;

import ru.inno.game.dto.PlayerDto;
import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repositories.GamesRepository;
import ru.inno.game.repositories.PlayersRepository;
import ru.inno.game.repositories.ShotsRepository;

import java.time.LocalDateTime;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// бизнес-логика
public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
//        // получили информацию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .firstPlayer(first)
                .secondPlayer(second)
                .firstPlayerShotsCount(0)
                .secondPlayerShotsCount(0)
                .secondsTimeAmount(0L)
                .build();
        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
            player = Player.builder()
                    .ip(ip)
                    .name(nickname)
                    .points(0)
                    .winsCounts(0)
                    .loseCounts(0)
                    .build();
            // сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был -> обновляем у него IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }

        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого стреляли из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
        Shot shot = Shot.builder()
                .dateTime(LocalDateTime.now())
                .game(game)
                .shooter(shooter)
                .target(target)
                .build();
        // увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getFirstPlayer().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        }
        // если стрелявший - второй игрок
        if (game.getSecondPlayer().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId, long seconds) {
        Game game = gamesRepository.findById(gameId);
        StatisticDto statisticDto = new StatisticDto();

        statisticDto.setId(gameId);
        statisticDto.setSecondsTimeAmount(seconds);
        game.setSecondsTimeAmount(seconds);

        PlayerDto firstPlayer = new PlayerDto();
        statisticDto.setFirstPlayer(firstPlayer);
        firstPlayer.setName(game.getFirstPlayer().getName());
        Integer firstPlayerShots = game.getFirstPlayerShotsCount();
        firstPlayer.setPoints(firstPlayerShots);
        firstPlayer.setTotalPoints(playersRepository.findByNickname(firstPlayer.getName()).getPoints());
        firstPlayer.setWinsCounts(playersRepository.findByNickname(firstPlayer.getName()).getWinsCounts());
        firstPlayer.setLoseCounts(playersRepository.findByNickname(firstPlayer.getName()).getLoseCounts());

        PlayerDto secondPlayer = new PlayerDto();
        statisticDto.setSecondPlayer(secondPlayer);
        secondPlayer.setName(game.getSecondPlayer().getName());
        Integer secondPlayerShots = game.getSecondPlayerShotsCount();
        secondPlayer.setPoints(secondPlayerShots);
        secondPlayer.setTotalPoints(playersRepository.findByNickname(secondPlayer.getName()).getPoints());
        secondPlayer.setWinsCounts(playersRepository.findByNickname(secondPlayer.getName()).getWinsCounts());
        secondPlayer.setLoseCounts(playersRepository.findByNickname(secondPlayer.getName()).getLoseCounts());

        if (firstPlayerShots.compareTo(secondPlayerShots) == 0) {
            statisticDto.setWinner("Ничья!");
        }
        if (firstPlayerShots.compareTo(secondPlayerShots) == -1) {
            statisticDto.setWinner(secondPlayer.getName());
            secondPlayer.setWinsCounts(secondPlayer.getWinsCounts() + 1);
            firstPlayer.setLoseCounts(firstPlayer.getLoseCounts() + 1);
            game.getSecondPlayer().setWinsCounts(secondPlayer.getWinsCounts());
            game.getFirstPlayer().setLoseCounts(firstPlayer.getLoseCounts());
        }
        if (firstPlayerShots.compareTo(secondPlayerShots) == 1) {
            statisticDto.setWinner(firstPlayer.getName());
            firstPlayer.setWinsCounts(firstPlayer.getWinsCounts() + 1);
            secondPlayer.setLoseCounts(secondPlayer.getLoseCounts() + 1);
            game.getFirstPlayer().setWinsCounts(firstPlayer.getWinsCounts());
            game.getSecondPlayer().setLoseCounts(secondPlayer.getLoseCounts());
        }
        gamesRepository.update(game);
        playersRepository.update(game.getFirstPlayer());
        playersRepository.update(game.getSecondPlayer());
        return statisticDto;
    }
}
