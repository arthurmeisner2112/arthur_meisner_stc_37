

create table player
(
    id     bigserial primary key,
    ip     varchar(15),
    name   varchar(20),
    points integer,
    wins   integer,
    loses  integer
);
create table game
(
    id                  bigserial primary key,
    date_time           varchar(100),
    first_player        bigint,
    second_player       bigint,
    first_player_shots  int,
    second_player_shots int,
    seconds_time_amount bigint,
    foreign key (first_player) references player(id),
    foreign key (second_player) references player(id)

);

create table shot
(
    id         bigserial primary key,
    date       date,
    game_id    bigint,
    shooter_id bigint,
    target_id  bigint
);
