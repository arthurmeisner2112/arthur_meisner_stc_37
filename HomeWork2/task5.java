import java.util.Arrays;

public class task5 {
	public static void main(String[] args) {
		int array[] = {5, 99, 86, 2, -100, 68, 0, -14, 4, 5, -27, 23};

		System.out.println("Initial array is " + Arrays.toString(array));

		boolean isSorted = false;
		int temp;

		while (!isSorted) {
			isSorted = true;
			for (int i = 0; i < array.length - 1; i++) {
				if (array[i] > array[i + 1]) {
					isSorted = false;

					temp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
					System.out.println("Array in Sorting " + Arrays.toString(array));
				}
			}
		}
		System.out.println("Sorted array is " + Arrays.toString(array));

	}
}