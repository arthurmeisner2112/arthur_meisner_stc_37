import java.util.Scanner;
import java.util.Arrays;

public class task2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];

			for (int i = 0; i < array.length; i++) {
			System.out.println("Insert next Number ");

			array[i] = scanner.nextInt();

		}

		System.out.println("Initial array is " + Arrays.toString(array));

		int arrayMiddle = array.length / 2;

		for (int j = 0; j < arrayMiddle; j++){

			int mirrorIndex = array.length -j - 1;
			int temp = array[mirrorIndex];
			array[mirrorIndex] = array[j];
			array[j] = temp;
		}

		System.out.println("Reversed array is " + Arrays.toString(array));


	}
}