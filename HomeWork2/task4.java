import java.util.Scanner;
import java.util.Arrays;

public class task4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];


		for (int i = 0; i < array.length; i++) {
			 System.out.println("Insert next Number ");

			array[i] = scanner.nextInt();
		}

		System.out.println("Initial array is " +  Arrays.toString(array));

		int min = array[0];
		int positionOfMin = 0;
		int max = array[0];
		int positionOfMax = 0;

		for (int j = 0; j < array.length; j++) {
			if (array[j] < min) {
				min = array[j];
				positionOfMin = j;
			}
			if (array[j] > max) {
				max = array[j];
				positionOfMax = j;
			}
		}

		int temp = array[positionOfMin];
		array[positionOfMin] = array[positionOfMax];
		array[positionOfMax] = temp;


		System.out.println("Minimal array value is " + min);
		System.out.println("Index of minimal array value is " + positionOfMin);
		System.out.println("Maximal array value is " + max);
		System.out.println("Index of maximal array value is " + positionOfMax);
		System.out.println("Final array is " +  Arrays.toString(array));


	}
}