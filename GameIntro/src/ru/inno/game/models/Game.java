package ru.inno.game.models;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

public class Game {
    private Long id;
    private LocalDateTime dateTime;
    private Player firstPlayer;
    private Player secondPlayer;
    private Integer firstPlayerShotsCount;
    private Integer secondPlayerShotsCount;
    private Long secondsTimeAmount;

    public Game(Long id, LocalDateTime dateTime, Player firstPlayer, Player secondPlayer, Integer firstPlayerShotsCount, Integer secondPlayerShotsCount, Long secondsTimeAmount) {
        this.id = id;
        this.dateTime = dateTime;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.firstPlayerShotsCount = firstPlayerShotsCount;
        this.secondPlayerShotsCount = secondPlayerShotsCount;
        this.secondsTimeAmount = secondsTimeAmount;
    }

    public Game(Long id, LocalDateTime dateTime, Integer firstPlayerShotsCount, Integer secondPlayerShotsCount, Long secondsTimeAmount) {
        this.id = id;
        this.dateTime = dateTime;
        this.firstPlayerShotsCount = firstPlayerShotsCount;
        this.secondPlayerShotsCount = secondPlayerShotsCount;
        this.secondsTimeAmount = secondsTimeAmount;
    }

    public Game(LocalDateTime dateTime, Player firstPlayer, Player secondPlayer, Integer firstPlayerShotsCount, Integer secondPlayerShotsCount, Long secondsTimeAmount) {
        this.dateTime = dateTime;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.firstPlayerShotsCount = firstPlayerShotsCount;
        this.secondPlayerShotsCount = secondPlayerShotsCount;
        this.secondsTimeAmount = secondsTimeAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(Player firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(Player secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public Integer getFirstPlayerShotsCount() {
        return firstPlayerShotsCount;
    }

    public void setFirstPlayerShotsCount(Integer firstPlayerShotsCount) {
        this.firstPlayerShotsCount = firstPlayerShotsCount;
    }

    public Integer getSecondPlayerShotsCount() {
        return secondPlayerShotsCount;
    }

    public void setSecondPlayerShotsCount(Integer secondPlayerShotsCount) {
        this.secondPlayerShotsCount = secondPlayerShotsCount;
    }

    public Long getSecondsTimeAmount() {
        return secondsTimeAmount;
    }

    public void setSecondsTimeAmount(Long secondsTimeAmount) {
        this.secondsTimeAmount = secondsTimeAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(dateTime, game.dateTime) && Objects.equals(firstPlayer, game.firstPlayer) && Objects.equals(secondPlayer, game.secondPlayer) && Objects.equals(firstPlayerShotsCount, game.firstPlayerShotsCount) && Objects.equals(secondPlayerShotsCount, game.secondPlayerShotsCount) && Objects.equals(secondsTimeAmount, game.secondsTimeAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, firstPlayer, secondPlayer, firstPlayerShotsCount, secondPlayerShotsCount, secondsTimeAmount);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Game.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("dateTime=" + dateTime)
                .add("firstPlayer=" + firstPlayer.getName())
                .add("secondPlayer=" + secondPlayer.getName())
                .add("firstPlayerShotsCount=" + firstPlayerShotsCount)
                .add("secondPlayerShotsCount=" + secondPlayerShotsCount)
                .add("secondsTimeAmount=" + secondsTimeAmount)
                .toString();
    }
}
