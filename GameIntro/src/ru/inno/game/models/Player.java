package ru.inno.game.models;

import java.util.Objects;

public class Player {
    private Long id;
    private String ip;
    private String name;
    private Integer points;
    private Integer winsCounts;
    private Integer loseCounts;

    public Player(String ip, String name, Integer Points, Integer winsCounts, Integer loseCounts) {
        this.ip = ip;
        this.name = name;
        this.points = Points;
        this.winsCounts = winsCounts;
        this.loseCounts = loseCounts;
    }

    public Player(Long id, String ip, String name, Integer points, Integer winsCounts, Integer loseCounts) {
        this.id = id;
        this.ip = ip;
        this.name = name;
        this.points = points;
        this.winsCounts = winsCounts;
        this.loseCounts = loseCounts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getWinsCounts() {
        return winsCounts;
    }

    public void setWinsCounts(Integer winsCounts) {
        this.winsCounts = winsCounts;
    }

    public Integer getLoseCounts() {
        return loseCounts;
    }

    public void setLoseCounts(Integer loseCounts) {
        this.loseCounts = loseCounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(ip, player.ip) && Objects.equals(name, player.name) && Objects.equals(points, player.points) && Objects.equals(winsCounts, player.winsCounts) && Objects.equals(loseCounts, player.loseCounts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, name, points, winsCounts, loseCounts);
    }
}
