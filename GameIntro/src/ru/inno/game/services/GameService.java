package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;

public interface GameService {
    /**
     * Метод вызывается для обеспечения начала игры. Если игрок с таким никнеймом есть, то мы работем с ним. Если такого нет, то создаём нового.
     * @param firstPlayerNickname Имя первого игрока
     * @param firstIp IP-адрес, с которого зашел первый игрок
     * @param secondIp IP - адрес, скоторого зашел второй игрок
     * @param secondPlayerNickname Имя второго игрока
     * @return индентификатор игры
     */
    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);

    /**
     * Фиксирует выстрел игроков (попавшие)
     * @param gameId индетификатор игры
     * @param shooterNickname имя первого игрока
     * @param targetNickname имя второго игрока
     */
    void shot(Long gameId, String shooterNickname, String targetNickname);

    StatisticDto finishGame(Long gameId);
}
