package ru.inno.game.services;

import ru.inno.game.dto.PlayerDto;
import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.LocalDateTime;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    public ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        //Получили информацию об обоих игроках
        Player first = checkIfExist(firstIp, firstPlayerNickname);
        Player second = checkIfExist(secondIp, secondPlayerNickname);
        //Создали игру
        Game game = new Game(LocalDateTime.now(), first, second, 0, 0, System.currentTimeMillis());
        //Сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExist(String firstIp, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        if (player == null) {
            // Создать пользователя
            player = new Player(firstIp, nickname, 0, 0, 0);
            playersRepository.save(player);
        } else {
            player.setIp(firstIp);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);
        shooter.setPoints(shooter.getPoints() + 1);

        if (game.getFirstPlayer().getName().equals(shooterNickname)) {
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        }

        if (game.getSecondPlayer().getName().equals(shooterNickname)) {
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }
        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId) {
        Game game = gamesRepository.findById(gameId);

        StatisticDto statisticDto = new StatisticDto();
        statisticDto.setSecondsTimeAmount((System.currentTimeMillis() - game.getSecondsTimeAmount()) / 1000);
        statisticDto.setId(gameId);
        game.setSecondsTimeAmount(statisticDto.getSecondsTimeAmount());
        PlayerDto firstPlayer = new PlayerDto();
        statisticDto.setFirstPlayer(firstPlayer);
        firstPlayer.setName(game.getFirstPlayer().getName());
        Integer firstPlayerShots = game.getFirstPlayerShotsCount();
        firstPlayer.setPoints(firstPlayerShots);
        firstPlayer.setTotalPoints(playersRepository.findByNickname(firstPlayer.getName()).getPoints());
        firstPlayer.setWinsCounts(playersRepository.findByNickname(firstPlayer.getName()).getWinsCounts());
        firstPlayer.setLoseCounts(playersRepository.findByNickname(firstPlayer.getName()).getLoseCounts());

        PlayerDto secondPlayer = new PlayerDto();
        statisticDto.setSecondPlayer(secondPlayer);
        secondPlayer.setName(game.getSecondPlayer().getName());
        Integer secondPlayerShots = game.getSecondPlayerShotsCount();
        secondPlayer.setPoints(secondPlayerShots);
        secondPlayer.setTotalPoints(playersRepository.findByNickname(secondPlayer.getName()).getPoints());
        secondPlayer.setWinsCounts(playersRepository.findByNickname(secondPlayer.getName()).getWinsCounts());
        secondPlayer.setLoseCounts(playersRepository.findByNickname(secondPlayer.getName()).getLoseCounts());

        if (firstPlayerShots.compareTo(secondPlayerShots) == 0) {
            statisticDto.setWinner("Ничья!");
        }
        if (firstPlayerShots.compareTo(secondPlayerShots) == -1) {
            statisticDto.setWinner(secondPlayer.getName());
            secondPlayer.setWinsCounts(secondPlayer.getWinsCounts() + 1);
            firstPlayer.setLoseCounts(firstPlayer.getLoseCounts() + 1);
            game.getSecondPlayer().setWinsCounts(secondPlayer.getWinsCounts());
            game.getFirstPlayer().setLoseCounts(firstPlayer.getLoseCounts());
        }
        if (firstPlayerShots.compareTo(secondPlayerShots) == 1) {
            statisticDto.setWinner(firstPlayer.getName());
            firstPlayer.setWinsCounts(firstPlayer.getWinsCounts() + 1);
            secondPlayer.setLoseCounts(secondPlayer.getLoseCounts() + 1);
            game.getFirstPlayer().setWinsCounts(firstPlayer.getWinsCounts());
            game.getSecondPlayer().setLoseCounts(secondPlayer.getLoseCounts());
        }
        gamesRepository.update(game);
        playersRepository.update(game.getFirstPlayer());
        playersRepository.update(game.getSecondPlayer());

        return statisticDto;

    }
}
