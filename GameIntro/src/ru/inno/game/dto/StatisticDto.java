package ru.inno.game.dto;

import ru.inno.game.models.Player;

import java.util.StringJoiner;

//информация об игре
public class StatisticDto {

    public Long id;
    public Long secondsTimeAmount;
    public PlayerDto firstPlayer;
    public PlayerDto secondPlayer;
    public String winner;

    @Override
    public String toString() {
        return '\n' + "Сервис статистики" +'\n' +
                "Игра с ID = " + id + '\n' +
                "Игра длилась " + secondsTimeAmount + " секунд" +'\n' +
                "Первый игрок " + firstPlayer +'\n' +
                "Второй игрок " + secondPlayer +'\n' +
                "Победитель " + winner + '\n';
    }

    public PlayerDto getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(PlayerDto firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public PlayerDto getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(PlayerDto secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSecondsTimeAmount() {
        return secondsTimeAmount;
    }

    public void setSecondsTimeAmount(Long secondsTimeAmount) {
        this.secondsTimeAmount = secondsTimeAmount;
    }
}
