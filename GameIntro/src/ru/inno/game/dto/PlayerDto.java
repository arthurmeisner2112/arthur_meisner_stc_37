package ru.inno.game.dto;

public class PlayerDto {
    private String name;
    private Integer points;
    private Integer totalPoints;
    private Integer winsCounts;
    private Integer loseCounts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

    public Integer getWinsCounts() {
        return winsCounts;
    }

    public void setWinsCounts(Integer winsCounts) {
        this.winsCounts = winsCounts;
    }

    public Integer getLoseCounts() {
        return loseCounts;
    }

    @Override
    public String toString() {
        return
                " " + name  +
                ", попаданий " + points +
                ", всего очков " + totalPoints +
                ", побед " + winsCounts +
                ", поражений " + loseCounts;
    }

    public void setLoseCounts(Integer loseCounts) {
        this.loseCounts = loseCounts;
    }
}
