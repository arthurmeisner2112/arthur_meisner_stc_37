package ru.inno.game.repository;

import ru.inno.game.models.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PlayersRepositoryFilesImpl implements PlayersRepository {

    private String dbFileName;
    private String sequenceFileName;
    private ArrayList<Player> players;


    public PlayersRepositoryFilesImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
        players = new ArrayList<>();
        readPlayersDb();


    }

    @Override
    public Player findByNickname(String nickName) {


        if (players.size() == 0) {
            return null;
        }
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).getName().equals(nickName)) {
                return players.get(i);
            }
        }
        return null;
    }

    @Override
    public void save(Player player) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true));
            player.setId(generateId());
            writer.write(player.getId() + "#" + player.getName() + "#" + player.getIp() + "#" + player.getPoints() + "#" + player.getWinsCounts() + "#" + player.getLoseCounts() + "\n");
            writer.close();
            players.add(player);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {

        if (player != null && player.getId() != null) {

            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName));
                for (Player player1 : players) {
                    writer.write(player1.getId() + "#" + player1.getName() + "#" + player1.getIp() + "#" + player1.getPoints() + "#" + player1.getWinsCounts() + "#" + player1.getLoseCounts() + "\n");
                }
                writer.close();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

    }

    private Long generateId() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            String lastGeneratedIdAsString = reader.readLine();
            Long id = Long.parseLong(lastGeneratedIdAsString);
            reader.close();
            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void readPlayersDb() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(dbFileName));
            String str = bufferedReader.readLine();

            while (str != null) {
                String[] arrStr = str.split("#");
                Player player = new Player(arrStr[2], arrStr[1], Integer.parseInt(arrStr[3]), Integer.parseInt(arrStr[4]), Integer.parseInt(arrStr[5]));
                player.setId(Long.parseLong(arrStr[0]));
                players.add(player);
                str = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


}
