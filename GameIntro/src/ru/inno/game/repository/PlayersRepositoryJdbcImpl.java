package ru.inno.game.repository;

import ru.inno.game.models.Player;

import static ru.inno.game.utils.jdbcUtil.closeJdbcObjects;

import javax.sql.DataSource;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_PLAYERS = "select * from player";
    //language=SQL
    private static final String SQL_INSERT_PLAYERS = "insert into player(ip,name,points,wins,loses) values (?,?,?,?,?)";
    //language=SQL
    private static final String SQL_FIND_PLAYER_BY_NAME = "select * from player where name = ?";
    //language=SQL
    private static final String SQL_UPDATE_PLAYER_BY_ID = "update player set ip = ?, points = ?, wins = ?, loses = ? where id = ?";

    private RowMapper<Player> playerRowMapper = row -> new Player(
            row.getLong("id"),
            row.getString("ip"),
            row.getString("name"),
            row.getInt("points"),
            row.getInt("wins"),
            row.getInt("loses")
    );


    private DataSource dataSource;
    private Map<String,Player> players;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        players = new HashMap<>();
        findAll();
    }

    public void findAll() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rows = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            rows = statement.executeQuery(SQL_SELECT_ALL_PLAYERS);
            while (rows.next()) {
                Player player = playerRowMapper.mapRow(rows);
                players.put(player.getName(), player);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
    }


    @Override
    public Player findByNickname(String nickName) {

        return players.get(nickName);

        /*
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;
        Player player = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NAME);
            statement.setString(1, nickName);

            rows = statement.executeQuery();
            if (rows.next()) {
                player = playerRowMapper.mapRow(rows);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return player;

         */
    }

    @Override
    public void save(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_PLAYERS, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getWinsCounts());
            statement.setInt(5, player.getLoseCounts());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()) {
                player.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
            players.put(player.getName(), player);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }

    @Override
    public void update(Player player) {
        if (players.containsKey(player.getName())){
            players.put(player.getName(), player);
        } else {
            System.err.println("Нельзя обновить несуществующего игрока");
        }

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_PLAYER_BY_ID);
            statement.setString(1, player.getIp());
         //   statement.setString(2, player.getName());
            statement.setInt(2, player.getPoints());
            statement.setInt(3, player.getWinsCounts());
            statement.setInt(4, player.getLoseCounts());
            statement.setLong(5, player.getId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }
}
