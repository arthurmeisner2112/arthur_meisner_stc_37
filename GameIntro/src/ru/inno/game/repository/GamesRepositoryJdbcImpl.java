package ru.inno.game.repository;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

import static ru.inno.game.utils.jdbcUtil.closeJdbcObjects;

public class GamesRepositoryJdbcImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into game(first_player, second_player, first_player_shots, second_player_shots, seconds_time_amount, date) values (?,?,?,?,?,?)";
    //language=SQL
    private static final String SQL_FIND_GAME_BY_ID = "select * from game where id = ?";
    //language=SQL
    private static final String SQL_UPDATE_GAME_BY_ID = "update game set first_player = ?, second_player = ?, first_player_shots = ?, second_player_shots = ?, seconds_time_amount = ? where id = ?";
    //language=SQL
    private static final String SQL_FIND_PLAYER_BY_ID = "select * from player where id =?";

    private RowMapper<Player> playerRowMapper = rows -> new Player(
            rows.getLong("id"),
            rows.getString("ip"),
            rows.getString("name"),
            rows.getInt("points"),
            rows.getInt("wins"),
            rows.getInt("loses")
    );

    private RowMapper<Game> gameRowMapper = row -> new Game(
            row.getLong("id"),
            row.getTimestamp("date").toLocalDateTime(),
            row.getInt("first_player_shots"),
            row.getInt("second_player_shots"),
            row.getLong("seconds_time_amount")
    );
    DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_GAME, statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, game.getFirstPlayer().getId());
            statement.setLong(2, game.getSecondPlayer().getId());
            statement.setInt(3, game.getFirstPlayerShotsCount());
            statement.setInt(4, game.getSecondPlayerShotsCount());
            statement.setLong(5, game.getSecondsTimeAmount());
            statement.setTimestamp(6, java.sql.Timestamp.valueOf(game.getDateTime()));
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()) {
                game.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override

    public Game findById(Long gameId) {

        Connection connection = null;
        PreparedStatement statement = null;
        PreparedStatement statement1 = null;
        //  PreparedStatement statement2 = null;
        ResultSet rows = null;
        ResultSet playerRows = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID);
            statement.setLong(1, gameId);
            rows = statement.executeQuery();
            rows.next();
            Long firstPlayerId = rows.getLong("first_player");
            Long secondPlayerId = rows.getLong("second_player");
            statement1 = connection.prepareStatement(SQL_FIND_PLAYER_BY_ID);
            statement1.setLong(1, firstPlayerId);
            playerRows = statement1.executeQuery();
            playerRows.next();
            Player firstPlayer = playerRowMapper.mapRow(playerRows);
            //  statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_ID);
            statement1.setLong(1, secondPlayerId);
            playerRows = statement1.executeQuery();
            playerRows.next();
            Player secondPlayer = playerRowMapper.mapRow(playerRows);
            Game game = gameRowMapper.mapRow(rows);
            game.setFirstPlayer(firstPlayer);
            game.setSecondPlayer(secondPlayer);
            return game;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
            try {
                statement1.close();
                //statement2.close();
            } catch (SQLException ignore) {
            }


        }


    /*    Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;
        ResultSet playerRows = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID);
            statement.executeQuery();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }

     */
    }


    @Override
    public void update(Game game) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID);

            statement.setLong(1, game.getFirstPlayer().getId());
            statement.setLong(2, game.getSecondPlayer().getId());
            statement.setInt(3, game.getFirstPlayerShotsCount());
            statement.setInt(4, game.getSecondPlayerShotsCount());
            statement.setLong(5, game.getSecondsTimeAmount());
            statement.setLong(6,game.getId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }


    }

    //private Player playerByIdCreator(Long id) {
    //
    //    return player;
    //}
}
