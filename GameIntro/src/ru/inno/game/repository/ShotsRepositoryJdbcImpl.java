package ru.inno.game.repository;

import ru.inno.game.models.Shot;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import static ru.inno.game.utils.jdbcUtil.closeJdbcObjects;

public class ShotsRepositoryJdbcImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_INSERT_SHOT = "insert into shot(date, game_id, shooter_id, target_id) values (?,?,?,?)";
    DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource){
        this.dataSource = dataSource;
    }
    @Override
    public void save(Shot shot) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_SHOT, statement.RETURN_GENERATED_KEYS);
            statement.setDate(1, java.sql.Date.valueOf(shot.getDateTime().toLocalDate()));
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getShooter().getId());
            statement.setLong(4, shot.getTarget().getId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()) {
                shot.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }
}
