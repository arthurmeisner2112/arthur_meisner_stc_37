package ru.inno.game.client.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.socket.SocketClient;

public class GameUtils {

    private static final int PLAYER_STEP = 5;
    private static final int DAMAGE = 5;
    private static final int MAX_X = 225;
    private static final int MIN_X = -225;

    private AnchorPane pane;
    private MainController controller;
    private SocketClient client;


    public void goLeft(Circle player) {
        if (player.getCenterX() >= MIN_X) {
            player.setCenterX(player.getCenterX() - PLAYER_STEP);
        }
    }

    public void goRight(Circle player) {
        if (player.getCenterX() <= MAX_X) {
            player.setCenterX(player.getCenterX() + PLAYER_STEP);
        }
    }

    public Circle createBulletFor(Circle player, boolean isEnemy) {
        Circle bullet = new Circle();
        bullet.setRadius(5);
        pane.getChildren().add(bullet);
        bullet.setCenterX(player.getCenterX() + player.getLayoutX());
        bullet.setCenterY(player.getCenterY() + player.getLayoutY());
        bullet.setFill(Color.ORANGE);

        int value;
        if (isEnemy) {
            value = 3;
        } else {
            value = -3;
        }
        final Circle target;
        final Label targetHp;
        if (!isEnemy) {
            target = controller.getEnemy();
            targetHp = controller.getHpEnemy();
        } else {
            target = controller.getPlayer();
            targetHp = controller.getHpPlayer();
        }





        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(16), animation -> {
            bullet.setCenterY(bullet.getCenterY() + value);

            if (bullet.isVisible() && isIntersects(bullet, target)) {
                createDamage(targetHp);
                bullet.setVisible(false);
                if (!isEnemy) {
                    client.sendMessage("DAMAGE");
                }
            }
        }));
        timeline.setCycleCount(160);
        timeline.play();
        return bullet;
    }

    private boolean isIntersects(Circle bullet, Circle player) {
        return bullet.getBoundsInParent().intersects(player.getBoundsInParent());
    }

    private void createDamage(Label hpLabel) {
        int hpPlayer = Integer.parseInt(hpLabel.getText());
        hpLabel.setText(String.valueOf(hpPlayer - DAMAGE));
        if (hpPlayer <= 5){
            client.sendMessage("exit");
        }
    }

    public void setPane(AnchorPane pane) {
        this.pane = pane;
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public void setClient(SocketClient client) {
        this.client = client;
    }
}
