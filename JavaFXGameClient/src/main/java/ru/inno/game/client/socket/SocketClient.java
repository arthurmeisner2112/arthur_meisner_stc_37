package ru.inno.game.client.socket;

import javafx.application.Platform;
import javafx.scene.text.TextFlow;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.utils.GameUtils;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

/**
 * 22.04.2021
 * 38. Sockets IO - Client
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// поток для получения сообщений с сервера
public class SocketClient extends Thread {
    // канал подключения
    private Socket socket;
    // стрим для отправления сообщений серверу
    private PrintWriter toServer;
    // стрим для получения сообщений от сервера
    private BufferedReader fromServer;

    private MainController controller;
    private GameUtils gameUtils;

    public SocketClient(MainController controller, String host, int port) {
        try {
            // создаем подключение к серверу
            socket = new Socket(host, port);
            // получаем стримы для чтения и записи
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // ждем сообщений от сервера
    @Override
    public void run() {
        while (true) {
            String messageFromServer;
            try {
                // прочитали сообщение с сервера
                messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    switch (messageFromServer) {
                        case "left" -> gameUtils.goLeft(controller.getEnemy());
                        case "right" -> gameUtils.goRight(controller.getEnemy());
                        case "shot" -> Platform.runLater(() -> gameUtils.createBulletFor(controller.getEnemy(), true));
                    }
                }
                if (messageFromServer.contains("Сервис статистики")) {
                    controller.getMessageField().setWrapText(true);
                    controller.getMessageField().setPrefRowCount(10);
                    controller.getMessageField().clear();
                    controller.getMessageField().appendText(messageFromServer);

                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    // отправляем сообщение серверу
    public void sendMessage(String message) {
        toServer.println(message);
    }

}
