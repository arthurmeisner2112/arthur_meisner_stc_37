
public class Main {

    public static void main(String[] args) {
        // создал список

    /*    InnoList list = new InnoLinkedList();

        list.add(7);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(15);
        list.add(20);
        list.add(-77);
        list.add(100);
        list.addToBegin(69);
        list.insert(6, 666);

        System.out.println(list.get(0)); // 7
        System.out.println(list.get(3)); // 12
        System.out.println(list.get(7)); // 100
        System.out.println(list.contains(10));

        InnoIterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }

      // InnoIterator iterator2 = list.iterator();

      // while (iterator2.hasNext()) {
      //     System.out.println(iterator2.next() + " ");
      // }

        list.remove(12);
        InnoIterator iterator3 = list.iterator();
        while (iterator3.hasNext()) {
            System.out.println(iterator3.next() + " ");
        }


*/

        InnoArrayList testList = new InnoArrayList();

        for (int i = 0; i <= 15; i++) {
            testList.add(10 + i);
        }

        InnoIterator iterator3 = testList.iterator();
        InnoIterator iterator4 = testList.iterator();
        InnoIterator iterator5 = testList.iterator();
        InnoIterator iterator6 = testList.iterator();
        InnoIterator iterator7 = testList.iterator();

        System.out.println("----");
        System.out.println("Начальный массив");
        while (iterator3.hasNext()) {
            System.out.print(iterator3.next() + " ");
        }
        System.out.println();
        testList.remove(10);
        testList.remove(46);

        System.out.println("----");
        System.out.println("Удаляем число 10 из массива");
        while (iterator4.hasNext()) {
            System.out.print(iterator4.next() + " ");
        }
        System.out.println();


        System.out.println("----");
        System.out.println("Проверка наличия числа 15 в массиве " + testList.contains(15));


        System.out.println("----");
        System.out.println("Замена числа под индексом 4 на 25");
        testList.insert(4, 25);
        while (iterator5.hasNext()) {
            System.out.print(iterator5.next() + " ");
        }
        System.out.println();

        testList.addToBegin(42);

        System.out.println("----");
        System.out.println("Добавляем в начало 42");
        while (iterator6.hasNext()) {
            System.out.print(iterator6.next() + " ");
        }
        System.out.println();

        testList.removeByIndex(9);
        System.out.println("----");
        System.out.println("Удаляем число под индексом 9");
        while (iterator7.hasNext()) {
            System.out.print(iterator7.next() + " ");
        }
        System.out.println();






    }
}



