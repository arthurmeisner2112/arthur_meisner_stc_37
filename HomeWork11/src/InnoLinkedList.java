/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Плюсы - неограниченный размер (ограничен VM), быстрое добавление/удаление в начало/конец
// Минусы - медленный доступ по  индексу
public class InnoLinkedList implements InnoList {

    private static class Node {
        int value;
        Node next;
        Node previous;

        Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;

    private int count;

    // get(7)
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next; // семь раз сделаю next
            }
            // возвращаем значение
            return current.value;
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        // TODO: реализовать
        Node current = first;
        if (index >= 0 && index < count) {
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
        } current.value = element;


    }

    @Override
    public void addToBegin(int element) {
        // TODO: реализовать
        Node newNode = new Node(element);
        first.previous = newNode;
        newNode.next = first;
        first = newNode;
        count++;
    }

    @Override
    public void removeByIndex(int index) {

        Node current = first;
        if (index > count | index == -1) {
            System.err.println("Такого индекса не существует.");
            return;
        }

        for (int i = 0; i < index; i++) {
            current = current.next;
        }
    //    current.value = current.next.value;
        current.next.previous = current.previous;
        if (current.previous != null) {
            current.previous.next = current.next;
        } else {
            first = current.next;
        }
        count--;
    }

//    @Override
//    public void add(int element) {
//        // новый узел для нового элемента
//        Node newNode = new Node(element);
//        // если список пустой
//        if (first == null) {
//            // новый элемент списка и есть самый первый
//            first = newNode;
//        } else {
//            // если элементы в списке уже есть, необходимо добраться до последнего
//            Node current = first;
//            // пока не дошли до узла, после которого ничего нет
//            while (current.next != null) {
//                // переходим к следующему узлу
//                current = current.next;
//            }
//            // дошли по последнего узла
//            // теперь новый узел - самый последний (следующий после предыдущего последнего)
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
            newNode.previous = last;

        }
        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {

        int index = searcher(element);
        removeByIndex(index);

    }

    @Override
    public boolean contains(int element) {
        // Начнём с первого элемента
        Node current = first;

        for (int i = 0; i < count; i++) { // Перебором идём до последнего
            if (current.value == element) { //Сравниваем значение в ноде с элементом
                return true;
            } else {
                current = current.next;
            } //Если не равно, продолжаем.
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private int searcher(int element) {
        Node current = first;

        for (int i = 0; i < count; i++) { // Перебором идём до последнего
            if (current.value == element) { //Сравниваем значение в ноде с элементом
                return i; //Если всё ок, возвращаем индекс ноды.
            } else {
                current = current.next;
            } //Если не равно, продолжаем.
        }
        return -1; //Если всё плохо, вернём -1 (Обработать и выкинуть ошибку)
    }

    private class InnoLinkedListIterator implements InnoIterator {

        // ссылка на текущий узел итератора
        private Node current;

        InnoLinkedListIterator() {
           // this.current = first;
        }

        @Override
        public int next() {
            if (current == null)  {
                this.current = first;
                return current.value;
            }
        //    int nextValue = current.next.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return current.value;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            if (current != null) {
                return current.next != null;
            }
            return first != null;
        }
    }
}
