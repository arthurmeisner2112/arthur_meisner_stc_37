public class Main2 {
    public static void main(String[] args) {

       // InnoArrayList testList = new InnoArrayList();
        InnoList testList = new InnoLinkedList();

        for (int i = 0; i <= 15; i++) {
            testList.add(10 + i);
        }


        System.out.println("----");
        System.out.println("Начальный массив");
        InnoIterator iterator = testList.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        testList.remove(10);
        testList.remove(46);

        System.out.println("----");
        System.out.println("Удаляем число 10 из массива");
        iterator = testList.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();


        System.out.println("----");
        System.out.println("Проверка наличия числа 15 в массиве " + testList.contains(15));

        System.out.println("----");
        System.out.println("Замена числа под индексом 4 на 25");
        testList.insert(4, 25);
        iterator = testList.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        testList.addToBegin(42);
        iterator = testList.iterator();
        System.out.println("----");
        System.out.println("Добавляем в начало 42");
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        testList.removeByIndex(9);
        System.out.println("----");
        System.out.println("Удаляем число под индексом 9");
        iterator = testList.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

    }
}
