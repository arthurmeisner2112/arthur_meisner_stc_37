import java.util.Scanner;

public class Task03 {
  public static void main(String[] args) {
    System.out.println("Enter Numbers: ");
    Scanner scanner = new Scanner(System.in);
    int currentNumber = scanner.nextInt();
    int product = 1;

    while (currentNumber != 0) {
      int currentNumberSum = 0;
      int sumCache = currentNumber; 
      while (sumCache != 0) {
        currentNumberSum = currentNumberSum + sumCache % 10;
        sumCache = sumCache / 10;
      }
      boolean prime = true; 
      for (int i = 2; i <= currentNumberSum / 2; i++) {
        int numberCache = currentNumberSum % i;
        if (numberCache == 0) {
          prime = false;
          break; 
        }
      }
      if (prime) {
        product = product * currentNumber;
      }
      currentNumber = scanner.nextInt();
    }

    System.out.println("Product of Prime Numbers = " + product);
    scanner.close();
  }
}