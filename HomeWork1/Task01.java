public class Task01 {
  public static void main(String[] args) {
    int number = 12345;
    System.out.printf("Digit sum = %d\n", digitSum(number));
  }

  public static int digitSum(int number) {
    return number == 0 ? 0 : number % 10 + digitSum(number / 10);
  }
}