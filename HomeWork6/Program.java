public class Program {

    public Program(String programName){
        this.programName = programName;
    }
    private String programName;

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }



}