import java.util.Random;

public class Channel {
    //Название канала

    private String channelName;
    // Массив програм
    private Program program[];


    // public TV(){
    //     this.channels = new Channel[10];
    //     for (int i = 0; i <channels.length; i++){
    //         channels[i] = new Channel(channelsNames[i]);
    //     }
    // }

    public String getChannelName() {
        return channelName;
    }
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

   public Program[] getProgram() {
       return program;
   }

    public Channel(String channelName) {
        this.channelName = channelName;
        this.program = new Program[10];
        for (int i = 0; i < program.length; i++) {
            program[i] = new Program(programNames[i]);
        }
    }

    String programNames[] = {
            "Программа 1",
            "Программа 2",
            "Программа 3",
            "Программа 4",
            "Программа 5",
            "Программа 6",
            "Программа 7",
            "Программа 8",
            "Программа 9",
            "Программа 10",
            "Программа 11",
            "Программа 12",
            "Программа 13",
            "Программа 14",
            "Программа 15",
            "Программа 16",
            "Программа 17",
            "Программа 18",
            "Программа 19",
            "Программа 20",
            "Программа 21",
            "Программа 22",
            "Программа 23",
            "Программа 24",
            "Программа 25",
    };

    // public void setProgram(Program[] program) {
    //     this.program = program;
    // }


    public String showProgram() {
        Random random = new Random();
        return program[random.nextInt(program.length)].getProgramName();
    }


}
