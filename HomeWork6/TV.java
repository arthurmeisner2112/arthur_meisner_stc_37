public class TV {

    private Channel channels[];

    public Channel[] getChannels() {
        return channels;
    }

    public TV(){
        this.channels = new Channel[10];
        for (int i = 0; i <channels.length; i++){
            channels[i] = new Channel(channelsNames[i]);
        }
    }

    public void setChannels(Channel[] channels) {
        this.channels = channels;
    }


    public void channelSelect(int channelNumber) {

        if (channelNumber >= channels.length) {
            return;
        }
        Channel channel = channels[channelNumber];
        System.out.println("Канал: " + channel.getChannelName() + " Программа: " + channel.showProgram());

        //return String.valueOf(channelNumber);
    }

   String channelsNames[] = {
          "Канал 1",
          "Канал 2",
          "Канал 3",
          "Канал 4",
          "Канал 5",
          "Канал 6",
          "Канал 7",
          "Канал 8",
          "Канал 9",
          "Канал 10",
          "Канал 11",
          "Канал 12",
          "Канал 13",
          "Канал 14",
          "Канал 15",
          "Канал 16",
          "Канал 17",
          "Канал 18",
          "Канал 19",
          "Канал 20",
          "Канал 21",
          "Канал 22",
          "Канал 23",
          "Канал 24",
          "Канал 25",
  };

}

