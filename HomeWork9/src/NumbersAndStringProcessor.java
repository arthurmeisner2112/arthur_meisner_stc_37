public class NumbersAndStringProcessor {

    private static final int MAX_PROCESSED_NUMBERS_AND_STRING_COUNT = 10;

    private int processedNumbers[];
    private int processedNumbersCount;
    private String processedStrings[];
    private int processedStringsCount;

    public NumbersAndStringProcessor(int[] processedNumbers, String[] processedStrings) {
        this.processedNumbers = processedNumbers;
        this.processedStrings = processedStrings;
    }
    public NumbersAndStringProcessor() {

    }

    void NumbersUtil() {
        this.processedNumbers = new int[MAX_PROCESSED_NUMBERS_AND_STRING_COUNT];
    }

    void StingUtil() {
        this.processedStrings = new String[MAX_PROCESSED_NUMBERS_AND_STRING_COUNT];
    }

    //todo: Сделать public int[] process(numbersProcessFunction function)
    public void numbersProcess(int number, NumbersProcessFunction function) {
        if (processedNumbersCount < MAX_PROCESSED_NUMBERS_AND_STRING_COUNT) {
            int processedNumber = function.process(number);
            saveNumber(processedNumber);
        } else {
            System.err.println("Кончилось место для обработки чисел");
        }


    }

    public int[] process(NumbersProcessFunction function) {
        int[] processedArray = new int[processedNumbers.length];
        for (int i = 0; i < processedArray.length; i++){
            processedArray[i] = function.process(processedNumbers[i]);
        };

        return processedArray;
    }

    //todo: Сделать public String[] process(stringProcessFunction function)
    public void stringProcess(String string, StringProcessFunction function) {
        if (processedStringsCount < MAX_PROCESSED_NUMBERS_AND_STRING_COUNT) {
            String processedString = function.process(string);
            saveString(processedString);
        } else {
            System.err.println("Кончилось место для обработки чисел");
        }

    }
    public String[] process(StringProcessFunction function) {
        String[] processedArray = new String[processedStrings.length];
        for (int i = 0; i < processedArray.length; i++){
            processedArray[i] = function.process(processedStrings[i]);
        };

        return processedArray;
    }

    private void saveNumber(int number) {
        processedNumbers[processedNumbersCount] = number;
        processedNumbersCount++;
    }

    private void saveString(String string) {
        processedStrings[processedStringsCount] = string;
        processedStringsCount++;
    }

}
