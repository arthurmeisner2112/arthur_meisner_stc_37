import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        // write your code here

        int[] initialNumbersArray = {102, 542, 35068, 1526};
        String[] initialStringArray = {"Marsel", "Arthur", "Alexander", "Test001", "Why 1 Are 2 You 3 mad 4"};

        NumbersAndStringProcessor numbersUtil = new NumbersAndStringProcessor(initialNumbersArray, initialStringArray);

        NumbersProcessFunction reverseNumber = number -> {
            int reversedNumber = 0;
            while (number != 0) {
                reversedNumber = reversedNumber * 10;
                reversedNumber = reversedNumber + number % 10;
                number = number / 10;
            }
           // System.out.println(reversedNumber);
            return reversedNumber;
        };
        //System.out.println(reverseNumber.process(54321));
        //numbersUtil.numbersProcess(12345, reverseNumber);
       //int[] reversedArray = numbersUtil.process(reverseNumber);
       /* for (int i = 0; i < reversedArray.length; i++) {
            System.out.println(reversedArray[i]);
        } */
        //Коммит для мердж  рекваста
        Arrays.stream(numbersUtil.process(reverseNumber)).forEach(System.out::println);



        NumbersProcessFunction delZeroes = number -> {
            int numberWithoutZeroes = 0;
            int numberArray[] = new int[20];

            for (int i = 0; i < numberArray.length; i++) {
                numberArray[i] = number % 10;
                number = number / 10;
            }

            for (int j = numberArray.length - 1; j >= 0; j--) {
                if (numberArray[j] != 0) {
                    numberWithoutZeroes = numberWithoutZeroes * 10;
                    numberWithoutZeroes = numberWithoutZeroes + numberArray[j];
                }
            }
            return numberWithoutZeroes;
        };
        //System.out.println(delZeroes.process(10002));
        System.out.println("-------");
        Arrays.stream(numbersUtil.process(delZeroes)).forEach(System.out::println);


        NumbersProcessFunction minusMinus = number -> {
            int mustEvenNumber = 0;
            int numberArray[] = new int[Integer.toString(number).length()];

            for (int i = 0; i < numberArray.length; i++) {
                numberArray[i] = number % 10;
                number = number / 10;
            }

            for (int j = numberArray.length - 1; j >= 0; j--) {
                if (numberArray[j] % 2 != 0 | numberArray[j] == 1) {
                    numberArray[j] = numberArray[j] - 1;
                }
            }
            for (int k = numberArray.length - 1; k >= 0; k--) {
                mustEvenNumber = mustEvenNumber * 10;
                mustEvenNumber = mustEvenNumber + numberArray[k];
            }
            //  System.out.println(Arrays.toString(numberArray));
            return mustEvenNumber;
        };
        System.out.println("-------");
        Arrays.stream(numbersUtil.process(minusMinus)).forEach(System.out::println);


        StringProcessFunction reverseString = string -> {

            return new StringBuilder(string).reverse().toString();
        };
        System.out.println("-------");
        Arrays.stream(numbersUtil.process(reverseString)).forEach(System.out::println);

        StringProcessFunction capsLockMagic = string -> {
            return string.toUpperCase();
        };
        System.out.println("-------");
        Arrays.stream(numbersUtil.process(capsLockMagic)).forEach(System.out::println);

        StringProcessFunction removeAllNumbers = string -> {
            String withOutNumbers = string.replaceAll("[0-9]","");
            return withOutNumbers;
        };
        System.out.println("-------");
        Arrays.stream(numbersUtil.process(removeAllNumbers)).forEach(System.out::println);

    }
}
