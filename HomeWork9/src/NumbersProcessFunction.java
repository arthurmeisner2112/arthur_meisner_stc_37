public interface NumbersProcessFunction {
    int process(int number);
}
