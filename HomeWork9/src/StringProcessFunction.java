public interface StringProcessFunction {
    String process(String string);
}
