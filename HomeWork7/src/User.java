

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;


    private User(Builder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        age = builder.age;
        isWorker = builder.isWorker;
    }

    static Builder builder(){
        return new Builder();
    }


    public static class Builder {

        private String firstName;
        private String lastName;
        private int age;
        private boolean isWorker;

        public Builder() {

        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }


        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            this.isWorker = isWorker;
            return this;
        }

        public User build() {
            User user = new User(this);
            return user;
        }


    }


    private User() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean getIsWorker() {
        return isWorker;
    }
}
