import java.util.Arrays;
import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        System.out.println("Эта програма выполняет бинарный поиск ");
        System.out.println("Тут захардкожен какой-то отсортированный массив.");
        System.out.println("C случайными числами от 0 до 20");
        System.out.println("Введите число число для поиска");

        Scanner scanner = new Scanner(System.in);
        int elementToSearch = scanner.nextInt();
        int[] array;
        array = new int[]{0, 2, 4, 5, 8, 11, 14, 16, 19, 20};
        int min = 0;
        int max = array.length - 1;
        binarySearch(array, elementToSearch, min, max);

        if (binarySearch(array, elementToSearch, min, max) == -1) {
            System.out.println("Такого числа нет!");
        } else {
            System.out.println("Такое число есть!");
        }
    }

    public static int binarySearch(int[] array, int elementToSearch, int min, int max) {


        if (max >= min) {
            int mid = min + (max - min) / 2;

            if (array[mid] == elementToSearch)
                return mid;

            if (array[mid] > elementToSearch)

                return binarySearch(array, elementToSearch, min, mid - 1);

            return binarySearch(array, elementToSearch, mid + 1, max);
        }
        return -1;
    }
}