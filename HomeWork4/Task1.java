import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        System.out.println("Эта програма проверяет является ли число степенью двойки. ");
        System.out.println("Програма выдаёт ложноположительный результат, если ввести 1 ");
        System.out.println("Введите число ");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        isPowerOfTwo(num);
        if (isPowerOfTwo(num) == true) {
            System.out.println("Число является степенью двойки");
        }
        if (isPowerOfTwo(num) == false) {
            System.out.println("Число не является степенью двойки");
        }
    }

    public static boolean isPowerOfTwo(int num) {
        if (num % 2 == 0) {
            return isPowerOfTwo(num / 2);
        } else {
            if (num == 1) {
                return true;
            } else {
                return false;
            }
        }
    }
}
